#include <iostream>
#include "Matrix3x3.h"

using namespace std;

int main()
{
	Matrix3x3 matrix = Matrix3x3(1, 2, 3, 1, 2, 1, 1, 1, 1);
	Matrix3x3 inversed = matrix.inversed();

	system("pause");
	return 0;
}